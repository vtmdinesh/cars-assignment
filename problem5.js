const inventory = require("./cars.js");
const sortCarModelOnYear = require("./problem4")





const filteringCarsOlderThan2000= () => {
    


    // finding number of cars made before the year 2000
    
    const carYearsList = sortCarModelOnYear()
    const filteredYears = []

    for (let i=0;i<carYearsList.length;i++){
        if (carYearsList[i] < 2000) {
        
            filteredYears.push(carYearsList[i])
        }
    }

    // Logging Length of filtered cars list

    console.log(filteredYears.length)
    
    
    // Finding the list of cars made before the year 2000
    let selectedCar;
    let filteredList =[];

    // Getting filtered cars list in an array 

    for (let i = 0;i < inventory.length; i++) {
        selectedCar = inventory[i];
        if (selectedCar.car_year < 2000) {

            filteredList.push(selectedCar);
      
        }
            
    }
    
    // Returning filtered cars list
    
    return filteredList;

}


module.exports = filteringCarsOlderThan2000;