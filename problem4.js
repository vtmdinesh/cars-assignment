const inventory = require("./cars.js");

const sortCarModelOnYear= () => {
    let selectedCar;
    let carYearsList =[];

    // Getting car years in an array 
    for (let i = 0;i < inventory.length; i++) {
        selectedCar = inventory[i];
       
        carYearsList.push(selectedCar.car_year);
           
    }
    // Returning filtered car years list
    return carYearsList;

}


module.exports = sortCarModelOnYear;