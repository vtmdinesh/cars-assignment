const inventory = require("./cars.js");

const findingLastCarInInventory = () => {

    // Finding index of last car
    const indexOfLastCar = (inventory.length) -1

    // Getting details of last car
    const lastCar = inventory[indexOfLastCar]

    // Logging car details in console
    console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`)
    
    }





module.exports = findingLastCarInInventory