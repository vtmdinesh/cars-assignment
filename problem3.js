const inventory = require("./cars.js");

const sortCarModelsName = () => {
    let selectedCar;
    let carModelList = [];

    // Getting car models in an array 

    for (let i = 0;i < inventory.length; i++) {
        selectedCar = inventory[i];
        carModelList.push(selectedCar.car_model)
    }

    // Sorting Car model list oin alphabetical order

    let temp=""
    
    for (let i = 0 ;i < carModelList.length;i++) {

        for(let j = i+1;j < carModelList.length-1; j++ ) {

            if (carModelList[j] < carModelList[i]){
                    temp = carModelList[j];
                    carModelList[j] = carModelList[i];
                    carModelList[i] = temp;
                
            }

        }
 
    }

    return carModelList


}


module.exports = sortCarModelsName