const filteringBMWandAudiCars = require ("../problem6.js");


// Filtering cars based on car_year

const filteredCarsList = filteringBMWandAudiCars();

// Converting car list into JSON format

const stringifiedCarLists = JSON.stringify(filteredCarsList);

console.log(stringifiedCarLists);
