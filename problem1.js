const inventory = require("./cars.js");


const findingCarDetailsWithId = (id) => {
    let selectedCar;

    for (let i = 0;i < inventory.length; i++) {
        selectedCar = inventory[i];
        
        if(selectedCar.id === id){
            // LOgging car details in console
            console.log (`Car ${selectedCar.id} is a ${selectedCar.car_make} ${selectedCar.car_model} ${selectedCar.car_year}`);
            break;
        }
    }

}



module.exports = findingCarDetailsWithId